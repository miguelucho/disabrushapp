<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Disabrush</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/flatpickr.min.css">
  <style>
    body {
      display: none;
    }
  </style>
</head>

<body>
  <section>
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-ingresar-perfil">
          <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-titulo-recordatorio"></h2>
          <div class="Conten-recordatorios">
            <div class="Conten-recordatorios-padre Seccion-padre">
              <a href="#!">
                <span data-colores="Texto" class="Colors Colorc-text-azul" data-i18n="disabrush-label-diario-recordatorio"></span>
              </a>
            </div>
            <div class="Conten-recordatorios-hijo Seccion-hijo">
              <form id="Diario" class="Forms">
                <label for="" data-i18n="disabrush-label-diario-manana"></label>
                <input type="text" data-colores="Borde" class="Colors Colorc-borde-azul Time-pickr Hora-1" placeholder="Ingresar hora" name="calendario[hora1]" required>
                <label for="" data-i18n="disabrush-label-diario-tarde"></label>
                <input type="text" data-colores="Borde" class="Colors Colorc-borde-azul Time-pickr Hora-2" placeholder="Ingresar hora" name="calendario[hora2]" required>
                <label for="" data-i18n="disabrush-label-diario-noche"></label>
                <input type="text" data-colores="Borde" class="Colors Colorc-borde-azul Time-pickr Hora-3" placeholder="Ingresar hora" name="calendario[hora3]" required>
                <p></p>
                <input data-colores="Fondo" type="submit" class="Btn Colors Colorc-bag-azul Text-blanco" value="Guardar">
              </form>
            </div>
            <div class="Conten-recordatorios-padre Seccion-padre">
              <a href="#!">
                <span data-colores="Texto" class="Colors Colorc-text-azul" data-i18n="disabrush-label-cambio-recordatorio"></span>
              </a>
            </div>
            <div class="Conten-recordatorios-hijo Seccion-hijo Conten-oculto">
              <form id="Cambio" class="Forms">
                <label for="" data-i18n="disabrush-label-cambio-fecha"></label>
                <input type="text" data-colores="Borde" class="Colors Colorc-borde-azul Date-pickr Fecha-cambio" placeholder="Ingresar fecha" name="calendario[fecha]" required>
                <p></p>
                <input data-colores="Fondo" type="submit" class="Btn Colors Colorc-bag-azul Text-blanco" value="Guardar">
              </form>
            </div>
            <div class="Conten-recordatorios-padre Seccion-padre">
              <a href="#!">
                <span data-colores="Texto" class="Colors Colorc-text-azul" data-i18n="disabrush-label-cita-recordatorio"></span>
              </a>
            </div>
            <div class="Conten-recordatorios-hijo Seccion-hijo Conten-oculto">
              <form id="Cita" class="Forms">
                <label for="" data-i18n="disabrush-label-cita-fecha"></label>
                <input type="text" data-colores="Borde" class="Colors Colorc-borde-azul Date-pickr Fecha-cita" placeholder="Ingresar fecha" name="calendario[fecha]" required>
                <p></p>
                <input data-colores="Fondo" type="submit" class="Btn Colors Colorc-bag-azul Text-blanco" value="Guardar">
              </form>
            </div>
          </div>
        </div>

        <!-- Menu inferior flotante -->
        <?php include("dist/libs/menu-inferior.php") ?>

      </div>
  </section>
  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.messagestore.js"></script>
  <script src="dist/js/flatpickr.js"></script>
  <script src="dist/js/flatpickr-es.js"></script>
  <script src="dist/js/calendario.js"></script>
</body>

</html>
