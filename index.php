<?php
$token = '';

if (isset($_REQUEST['tokenid'])) {
  $token = $_REQUEST['tokenid'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Disabrush</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <style>
    .Seccion-idioma,
    .Seccion-color,
    .Seccion-foto,
    .Seccion-acciones,
    .Seccion-anterior {
      display: none;
    }

    .Seccion-activa {
      display: block;
    }
  </style>
</head>

<body>
  <section class="Seccion-idioma" data-seccion="idioma">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-idioma">
          <h2 class="Titul-h2 Text-center Colors Colorc-text-azul"><span data-i18n="disabrush-titulo-bienvenida-1"></span> <br> <span data-i18n="disabrush-titulo-bienvenida-2"></span></h2>
          <div class="Conten-idioma-sec">
            <div class="Conten-idioma-cuadro Colors Colorc-borde-azul">
              <a href="#!" class="Select-idioma" data-idioma="es">
                <img src="dist/assets/images/colombia.svg" alt="">
                <span>ES / Español</span>
              </a>
            </div>
            <div class="Conten-idioma-cuadro Colors">
              <a href="#!" class="Select-idioma" data-idioma="en">
                <img src="dist/assets/images/estados-unidos.svg" alt="">
                <span>EN / English</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Seleccionar colores -->
  <section class="Seccion-color" data-seccion="color">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-colores">
          <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-titulo-color"></h2>
          <p class="Parrafo Text-center" data-i18n="disabrush-texto-color"></p>
          <div class="Conten-colores-int">
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-amarillo">
                <div class="Conten-colores-bloque-seccion Colorf-amar">
                </div>
              </a>
            </div>
            <div class="Conten-colores-bloque Colors Colorc-borde-azul">
              <a href="#!" class="Btn-color" id="C-azul">
                <div class=" Conten-colores-bloque-seccion Colorf-azul">
                </div>
              </a>
            </div>
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-rojo">
                <div class="Conten-colores-bloque-seccion Colorf-rojo">
                </div>
              </a>
            </div>
          </div>
          <div class="Conten-colores-int">
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-verde">
                <div class="Conten-colores-bloque-seccion Colorf-verde">
                </div>
              </a>
            </div>
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-morado">
                <div class="Conten-colores-bloque-seccion Colorf-morado">
                </div>
              </a>
            </div>
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-naranja">
                <div class="Conten-colores-bloque-seccion Colorf-naranja">
                </div>
              </a>
            </div>
          </div>
          <div class="Conten-colores-int">
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-cafe">
                <div class="Conten-colores-bloque-seccion Colorf-cafe">
                </div>
              </a>
            </div>
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-gris">
                <div class="Conten-colores-bloque-seccion Colorf-gris">
                </div>
              </a>
            </div>
            <div class="Conten-colores-bloque">
              <a href="#!" class="Btn-color" id="C-negro">
                <div class="Conten-colores-bloque-seccion Colorf-negro">
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Sección para asignar un nombre y avatar -->

  <section class="Seccion-foto" data-seccion="foto">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-ingresar-perfil">
          <p class="Parrafo Text-center" data-i18n="disabrush-titulo-perfil"></p>
          <div class="Conten-ingresar-perfil-int">
            <form id="Perfil" class="Forms">
              <label for="" data-i18n="disabrush-label-nombre-perfil"></label>
              <input type="text" data-colores="Borde" name="registro[nombre]" class="Colors Colorc-borde-azul Nombre-perfil" placeholder="Nombre" required>
              <label data-i18n="disabrush-label-imagen-perfil"></label>
              <div class="Conten-avatar">
                <div class="Conten-avatar-int">
                  <div class="Conten-avatar-int-sec Colors Colorc-borde-azul Avatar-seleccionar" data-avatartipo="chico">
                    <img src="dist/assets/images/chico-1.png" alt="">
                  </div>
                  <div class="Conten-avatar-int-sec Colors Avatar-seleccionar" data-avatartipo="nina">
                    <img src="dist/assets/images/nina-1.png" alt="">
                  </div>
                </div>
              </div>
              <!-- <label for="" data-i18n="disabrush-label-foto-perfil"></label>
              <input type="file" class="Cp-oculto File-foto" id="file-imagen">
              <a href="#!" data-colores="Fondo" class="Btn Text-blanco Text-center Colors Colorc-bag-azul Tomar-foto"><i class="icon-camera"> </i> <span data-i18n="disabrush-label-btn-perfil"></span></a> -->
              <input type="hidden" name="registro[token_celular]" value="<?php echo $token ?>">
              <input type="submit" style="display:none" value="">
            </form>
          </div>
        </div>
      </div>

      <div class="modal micromodal-slide" id="modal-1" aria-hidden="true">
        <div class="modal__overlay" tabindex="-1">
          <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <header class="modal__header">
              <h2 class="modal__title Colors Colorc-text-azul" id="modal-1-title" data-i18n="disabrush-label-imagen-perfil">
              </h2>
            </header>
            <main class="modal__content" id="modal-1-content">
              <div class="Conten-avatar-int">
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="1">
                  <img src="dist/assets/images/chico-1.png" alt="">
                </div>
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="2">
                  <img src="dist/assets/images/chico-2.png" alt="">
                </div>
              </div>
              <div class="Conten-avatar-int">
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="3">
                  <img src="dist/assets/images/chico-3.png" alt="">
                </div>
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="4">
                  <img src="dist/assets/images/chico-4.png" alt="">
                </div>
              </div>
              <div class="Conten-avatar-int">
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="5">
                  <img src="dist/assets/images/chico-5.png" alt="">
                </div>
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="6">
                  <img src="dist/assets/images/chico-6.png" alt="">
                </div>
              </div>
            </main>
            <footer class="modal__footer">
              <button class="Btn Text-blanco Colorc-bag-gris" data-micromodal-close aria-label="Cerrar" data-i18n="disabrush-label-modal-1"></button>
              <button class="Btn Text-blanco Colors Colorc-bag-azul Seleccionar-avatar" data-i18n="disabrush-label-modal-2"></button>
            </footer>
          </div>
        </div>
      </div>

      <div class="modal micromodal-slide" id="modal-2" aria-hidden="true">
        <div class="modal__overlay" tabindex="-1">
          <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
            <header class="modal__header">
              <h2 class="modal__title Colors Colorc-text-azul" id="modal-1-title" data-i18n="disabrush-label-imagen-perfil">
              </h2>
            </header>
            <main class="modal__content" id="modal-1-content">
              <div class="Conten-avatar-int">
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="1">
                  <img src="dist/assets/images/nina-1.png" alt="">
                </div>
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="2">
                  <img src="dist/assets/images/nina-2.png" alt="">
                </div>
              </div>
              <div class="Conten-avatar-int">
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="3">
                  <img src="dist/assets/images/nina-3.png" alt="">
                </div>
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="4">
                  <img src="dist/assets/images/nina-4.png" alt="">
                </div>
              </div>
              <div class="Conten-avatar-int">
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="5">
                  <img src="dist/assets/images/nina-5.png" alt="">
                </div>
                <div class="Conten-avatar-int-sec Avatar-img" data-avatar="6">
                  <img src="dist/assets/images/nina-6.png" alt="">
                </div>
              </div>
            </main>
            <footer class="modal__footer">
              <button class="Btn Text-blanco Colorc-bag-gris" data-micromodal-close aria-label="Cerrar" data-i18n="disabrush-label-modal-1"></button>
              <button class="Btn Text-blanco Colors Colorc-bag-azul Seleccionar-avatar" data-i18n="disabrush-label-modal-2"></button>
            </footer>
          </div>
        </div>
      </div>
  </section>

  <div class="Conten-bloque-btn Seccion-acciones">
    <div class="Conten-bloque-btn-sec">
      <div class="Conten-bloque-lado">
        <a href="#!" class="Btn Colorc-bag-gris Text-blanco Seccion-anterior" data-i18n="disabrush-btn-anterior"></a>
      </div>
      <div class="Conten-bloque-lado">
        <a href="#!" data-colores="Fondo" class="Btn Colors Colorc-bag-azul Text-blanco Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a>
      </div>
    </div>
  </div>
  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/micromodal.min.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.messagestore.js"></script>
  <script src="dist/js/cambio-de-color.js?<?php echo time()  ?>"></script>

</body>

</html>
