$(document).ready(function () {
  var avatar_content, avatar_tipo, avatar_id, seccion_app, idioma_app, calificacion_app;

  colorapp = localStorage.getItem('color-app');
  avatar_tipo = localStorage.getItem('avatar-app');
  avatar_id = localStorage.getItem('avatarid-app');
  seccion_app = localStorage.getItem('seccion-app');
  idioma_app = localStorage.getItem('idioma-app');
  usuarioid_app = localStorage.getItem('usuarioid-app');
  calificacion_app = localStorage.getItem('calificacion-app');

  if (colorapp) {
    CambiardeFondo(colorapp);
  } else {
    colorapp = 'azul';
    localStorage.setItem("color-app", colorapp);
  }

  if (!idioma_app) {
    idioma_app = 'es';
    localStorage.setItem("idioma-app", idioma_app);
  }

  if (calificacion_app) {
    estrella = calificacion_app;
    $.each($('.Conten-estrellas-int span'), function () {
      if (estrella > 0) {
        $(this).removeClass('icon-star-empty').addClass('icon-star-full');
      }

      estrella--;
    });
  }

  var i18n = $.i18n();

  i18n.locale = idioma_app;

  i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
    $('*').i18n();
    $('.Nombre-contacto').attr('placeholder', $.i18n('disabrush-placeholder-nombre-contacto'));
    $('.Email-contacto').attr('placeholder', $.i18n('disabrush-placeholder-email-contacto'));
    $('.Mensaje-contacto').attr('placeholder', $.i18n('disabrush-placeholder-mensaje-contacto'));
    $('[type=submit]', '#Contacto').val($.i18n('disabrush-btn-enviar'));

    $('body').show();
  });

  $('.Conten-estrellas-int span').on('click', function () {
    if (!$(this).hasClass('icon-star-empty')) {
      $('.Conten-estrellas-int span').removeClass('icon-star-full').addClass('icon-star-empty');
    } else {
      $(this).removeClass('icon-star-empty').addClass('icon-star-full');
      $(this).prevAll('.icon-star-empty').removeClass('icon-star-empty').addClass('icon-star-full');

      if ($('.icon-star-full').length > 0) {
        $.post('dist/libs/ac_contacto', {
          'contacto[opc]': 'Envio-calificacion',
          'contacto[calificacion]': $('.icon-star-full').length,
          'contacto[token]': usuarioid_app
        }, function (data) {
          if (data.status == true) {
            localStorage.setItem("calificacion-app", $('.icon-star-full').length);
            notification('success', data.msg);
          } else {
            notification('error', data.msg);
          }
        }, 'json');
      }
    }
  });


  $('#Contacto').on('submit', function (e) {
    e.preventDefault();
    data = $(this).serializeArray();
    data.push({
      name: 'contacto[idioma]',
      value: idioma_app
    }, {
      name: 'contacto[calificacion]',
      value: 'Envio-correo'
    });

    $.post('dist/libs/ac_contacto', data, function (data) {
      if (data.status == true) {
        $('input').not('[type=submit]').val('');
        notification('success', data.msg);
      } else {
        notification('error', data.msg);
      }
    }, 'json');
  });

  function CambiardeFondo(nuevocolor) {
    $.each($(".Colors"), function () {

      if ($(this).is("[class*='Colorc-bag-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-bag-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-text-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-text-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-text-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-borde-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-borde-' + nuevocolor);
      }

    })

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $('#C-' + nuevocolor).parent().addClass('Colorc-borde-' + nuevocolor);
  }

  $('.Conten-bloque-menuabajo-sec a').on('click', function () {
    $('body').append('<div class="Fondo-cargando"><div class="Cargando-spin"><div class="lds-dual-ring"></div></div></div>');
  });

  function notification(t, m) {
    var n = new Noty({
      type: t,
      text: m,
      layout: 'center',
      theme: 'relax',
      modal: true,
      closeWith: ['click'],
      progressBar: false,
      timeout: 1500,
    });
    n.show();
  }

});
