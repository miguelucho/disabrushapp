$(document).ready(function () {
  var colorapp, idioma_app;

  colorapp = localStorage.getItem('color-app');
  idioma_app = localStorage.getItem('idioma-app');

  if (colorapp) {
    CambiardeFondo(colorapp);
  } else {
    colorapp = 'azul';
    localStorage.setItem("color-app", colorapp);
  }

  if (!idioma_app) {
    idioma_app = 'es';
    localStorage.setItem("idioma-app", idioma_app);
  }

  var i18n = $.i18n();

  i18n.locale = idioma_app;

  i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
    $('*').i18n();
    $('body').show();
  });

  function CambiardeFondo(nuevocolor) {
    $.each($(".Colors"), function () {

      if ($(this).is("[class*='Colorc-bag-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-bag-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-text-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-text-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-text-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-borde-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-borde-' + nuevocolor);
      }

    })

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $('#C-' + nuevocolor).parent().addClass('Colorc-borde-' + nuevocolor);
  }

  $('.Conten-bloque-menuabajo-sec a').on('click', function () {
    $('body').append('<div class="Fondo-cargando"><div class="Cargando-spin"><div class="lds-dual-ring"></div></div></div>');
  });

});
