$(document).ready(function () {
  var avatar_content, avatar_tipo, avatar_id, seccion_app, usuario_app, usuariofoto_app, usuarioid_app;
  var formData = new FormData();
  MicroModal.init();

  colorapp = localStorage.getItem('color-app');
  avatar_tipo = localStorage.getItem('avatar-app');
  avatar_id = localStorage.getItem('avatarid-app');
  seccion_app = localStorage.getItem('seccion-app');
  usuario_app = localStorage.getItem('usuario-app');
  usuariofoto_app = localStorage.getItem('usuariofoto-app');
  usuarioid_app = localStorage.getItem('usuarioid-app');
  idioma_app = localStorage.getItem('idioma-app');

  var i18n = $.i18n();

  i18n.locale = idioma_app;

  i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
    $('*').i18n();
    $('.Nombre-perfil').attr('placeholder', $.i18n('disabrush-placeholder-nombre-perfil'));
    $('[type=submit]', '#Perfil').val($.i18n('disabrush-btn-guardar'));

    $('body').show();
  });

  if (colorapp) {
    CambiardeFondo(colorapp);
  } else {
    colorapp = 'azul';
    localStorage.setItem("color-app", colorapp);
  }


  $('.Select-idioma').parent().removeClass(function (index, css) {
    return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
  });

  $('*[data-idioma="' + idioma_app + '"]').parent().addClass('Colorc-borde-' + colorapp);

  $('.Select-idioma').on('click', function () {
    $('.Select-idioma').parent().removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $(this).parent().addClass('Colorc-borde-' + colorapp);

    idioma_app = $(this).attr('data-idioma');
    localStorage.setItem("idioma-app", idioma_app);
    i18n.locale = $(this).attr('data-idioma');
    cambioIdioma();
  });

  if (usuariofoto_app) {
    $('.Img-usuario').attr('src', 'dist/' + usuariofoto_app);
  } else {
    if (!avatar_tipo) {
      avatar_tipo = 'chico';
      localStorage.setItem("avatar-app", avatar_tipo);
    }

    if (!avatar_id) {
      avatar_id = '1';
      localStorage.setItem("avatarid-app", avatar_id);
    }
  }

  $('.Nombre-perfil').val(usuario_app);

  if (avatar_tipo == 'chico') {
    $('*[data-avatartipo="chico"]').addClass('Colorc-borde-' + colorapp).find('img').attr('src', 'dist/assets/images/' + avatar_tipo + '-' + avatar_id + '.png');
  } else {
    $('*[data-avatartipo="nina"]').addClass('Colorc-borde-' + colorapp).find('img').attr('src', 'dist/assets/images/' + avatar_tipo + '-' + avatar_id + '.png');
  }

  $('.Btn-color').on('click', function () {
    guardarcolorboton = $(this).prop('id').split('-');
    colorapp = guardarcolorboton[1];

    localStorage.setItem("color-app", colorapp);

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $(this).parent().addClass('Colorc-borde-' + colorapp);

    CambiardeFondo(colorapp);

  });

  $('.Avatar-seleccionar').on('click', function () {
    $('.Avatar-seleccionar').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $(this).addClass('Colorc-borde-' + colorapp);

    avatar_content = $(this);
    avatar_tipo = $(this).attr('data-avatartipo');

    if (avatar_tipo == 'chico') {
      MicroModal.show('modal-1');
    } else {
      MicroModal.show('modal-2');
    }
  });

  $('.Avatar-img').on('click', function () {
    $('.Avatar-img').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    }).removeClass('Avatar-activo');

    $(this).addClass('Colorc-borde-' + colorapp);
    $(this).addClass('Avatar-activo');
  });

  $('.Seleccionar-avatar').on('click', function () {
    avatar_id = $('.Avatar-activo').attr('data-avatar');
    $('img', avatar_content).attr('src', 'dist/assets/images/' + avatar_tipo + '-' + avatar_id + '.png');
    MicroModal.close('modal-1');
    MicroModal.close('modal-2');
    $('.Img-usuario').attr('src', '');
  });

  $('.Tomar-foto').on('click', function () {
    $('.File-foto').click();
  });

  $('#Perfil').on('submit', function (e) {
    e.preventDefault();

    // if ($('.File-foto').val() != '') {
    //   inputFileImage = document.getElementById('file-imagen');
    //   file = inputFileImage.files[0];
    //   formData.append('foto', file);
    // } else {
    formData.append('registro[imagen]', avatar_tipo + '-' + avatar_id + '.png');
    // }

    data = $(this).serializeArray();
    $.each(data, function (key, input) {
      formData.append(input.name, input.value);
    });

    formData.append('registro[token]', usuarioid_app);
    formData.append('registro[idioma]', idioma_app);
    formData.append('registro[color]', colorapp);

    formData.append('registro[opc]', 'perfil');

    $.ajax('dist/libs/ac_registro', {
      method: "POST",
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      dataType: 'json',
      success: function (data) {
        if (data.status == true) {
          $('.File-foto').val('');
          localStorage.setItem("usuariofoto-app", data.foto);

          if (data.foto == '') {
            localStorage.setItem("avatar-app", avatar_tipo);
            localStorage.setItem("avatarid-app", avatar_id);
          } else {
            $('.Img-usuario').attr('src', 'dist/' + data.foto);
          }

          localStorage.setItem("usuario-app", $('.Nombre-perfil').val());

          notification('success', data.msg);
          // location.reload();
        } else {
          notification('error', data.msg);
        }
      }
    });

  });

  function cambioIdioma() {
    i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
      $('*').i18n();
      $('.Nombre-perfil').attr('placeholder', $.i18n('disabrush-placeholder-nombre-perfil'));
      $('[type=submit]', '#Perfil').val($.i18n('disabrush-btn-guardar'));

    });
  }

  function CambiardeFondo(nuevocolor) {
    $.each($(".Colors"), function () {

      if ($(this).is("[class*='Colorc-bag-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-bag-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-text-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-text-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-text-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-borde-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-borde-' + nuevocolor);
      }

    })

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $('#C-' + nuevocolor).parent().addClass('Colorc-borde-' + nuevocolor);

    if (nuevocolor == 'gris' || nuevocolor == 'negro') {
      $('.Conten-avatar-int img').css('filter', 'grayscale(1)');
    } else {
      $('.Conten-avatar-int img').css('filter', '');
    }
  }

  $('.Conten-bloque-menuabajo-sec a').on('click', function () {
    $('body').append('<div class="Fondo-cargando"><div class="Cargando-spin"><div class="lds-dual-ring"></div></div></div>');
  });

  function notification(t, m) {
    var n = new Noty({
      type: t,
      text: m,
      layout: 'center',
      theme: 'relax',
      modal: true,
      closeWith: ['click'],
      progressBar: false,
      timeout: 1500,
    });
    n.show();
  }

});
