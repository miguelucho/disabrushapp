$(document).ready(function () {
  var audioElement = document.createElement('audio');
  var audio_paso = document.createElement('audio');
  colorapp = localStorage.getItem('color-app');
  avatar_tipo = localStorage.getItem('avatar-app');
  avatar_id = localStorage.getItem('avatarid-app');
  idioma_app = localStorage.getItem('idioma-app');

  var i18n = $.i18n();

  i18n.locale = idioma_app;

  i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
    $('*').i18n();
    $('body').show();
  });

  if (colorapp) {
    CambiardeFondo(colorapp);
  } else {
    colorapp = 'azul';
    localStorage.setItem("color-app", colorapp);
  }

  if (!avatar_tipo) {
    avatar_tipo = 'chico';
    localStorage.setItem("avatar-app", avatar_tipo);
  }
  if (!avatar_id) {
    avatar_id = '1';
    localStorage.setItem("avatarid-app", avatar_id);
  }

  $('.Seccion-siguiente').on('click', function () {
    if (!$(this).attr('disabled')) {
      $('.Seccion-activa').removeClass('Seccion-activa').next('section').addClass('Seccion-activa');

      if ($('.Seccion-activa').find('.Crometro-sec2').length) {
        var cronometro = $('.Seccion-activa').find('.Crometro-sec2');
        var tiempo_restante = cronometro.attr('data-tiempo');
        var timer = setInterval(function () {
          $('span', cronometro).text('00: ' + (tiempo_restante < 11 ? '0' : '') + --tiempo_restante);
          if (tiempo_restante == 0) {
            clearInterval(timer);
            if ($('.Seccion-activa a.Seccion-siguiente').hasClass('Colorc-bag-gris')) {
              $('.Seccion-activa a.Seccion-siguiente').removeClass(function (index, css) {
                return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
              }).addClass('Colorc-bag-' + colorapp).attr('disabled', false);
            }
          }
        }, 1000);
      }

      // audioElement.volume = 0.03;
      audioElement.pause();

      if ($('.Seccion-activa').attr('data-paso')) {
        audio_paso.currentTime = 0;
        audio_paso.setAttribute('src', 'dist/assets/audio/' + idioma_app + '/paso-' + $('.Seccion-activa').attr('data-paso') + '.mp3');

        audio_paso.addEventListener("canplay", function () {
          this.play();
        });

        audio_paso.addEventListener('ended', function () {
          setTimeout(function () {
            audioElement.play();
            // audioElement.volume = 1.0;
          }, 300);
        }, false);
      }
    }

    if (!$(this).hasClass('Colorc-bag-gris')) {
      $('.Seccion-activa a.Seccion-siguiente').removeClass(function (index, css) {
        return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
      }).addClass('Colorc-bag-gris').attr('disabled', true);
    }
  });

  $('.Seccion-anterior').on('click', function () {
    $('.Seccion-activa').removeClass('Seccion-activa').prev('section').addClass('Seccion-activa');
  });

  $('.Seleccion-cancion').on('click', function () {
    audioElement.currentTime = 0;
    audioElement.setAttribute('src', 'dist/assets/audio/canciones/' + $(this).attr('data-src') + '.mp3');

    audioElement.addEventListener('ended', function () {
      this.play();
    }, false);

    audioElement.addEventListener("canplay", function () {
      this.play();
    });

    $('.Seleccion-cancion i').removeClass('icon-stop').addClass('icon-play2');

    $(this).find('i').removeClass('icon-play2').addClass('icon-stop');

    if ($('.Seccion-activa a.Seccion-siguiente').hasClass('Colorc-bag-gris')) {
      $('.Seccion-activa a.Seccion-siguiente').removeClass(function (index, css) {
        return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
      }).addClass('Colorc-bag-' + colorapp).attr('disabled', false);
    }
  });

  $('.Seccion-terminar').on('click', function () {
    location.reload();
  });

  function CambiardeFondo(nuevocolor) {
    $.each($(".Colors"), function () {

      if ($(this).is("[class*='Colorc-bag-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-bag-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-text-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-text-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-text-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-borde-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-borde-' + nuevocolor);
      }

    })

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $('#C-' + nuevocolor).parent().addClass('Colorc-borde-' + nuevocolor);

    if (nuevocolor == 'gris' || nuevocolor == 'negro') {
      $('.Conten-orden-superior-int img').css('filter', 'grayscale(1)');
    } else {
      $('.Conten-orden-superior-int img').css('filter', '');
    }
  }

  $('.Conten-bloque-menuabajo-sec a').on('click', function () {
    $('body').append('<div class="Fondo-cargando"><div class="Cargando-spin"><div class="lds-dual-ring"></div></div></div>');
  });

  function notification(t, m) {
    var n = new Noty({
      type: t,
      text: m,
      layout: 'center',
      theme: 'relax',
      modal: true,
      closeWith: ['click'],
      progressBar: false,
      timeout: 1500,
    });
    n.show();
  }

});
