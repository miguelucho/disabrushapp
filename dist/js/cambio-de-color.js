$(document).ready(function () {
  var avatar_content, avatar_tipo, avatar_id, seccion_app, idioma_app;
  var formData = new FormData();
  MicroModal.init();

  colorapp = localStorage.getItem('color-app');
  avatar_tipo = localStorage.getItem('avatar-app');
  avatar_id = localStorage.getItem('avatarid-app');
  seccion_app = localStorage.getItem('seccion-app');
  idioma_app = localStorage.getItem('idioma-app');

  if (colorapp) {
    CambiardeFondo(colorapp);
  } else {
    colorapp = 'azul';
    localStorage.setItem("color-app", colorapp);
  }

  if (!idioma_app) {
    idioma_app = 'es';
    localStorage.setItem("idioma-app", idioma_app);
  }

  $('.Select-idioma').parent().removeClass(function (index, css) {
    return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
  });

  $('*[data-idioma="' + idioma_app + '"]').parent().addClass('Colorc-borde-' + colorapp);

  var i18n = $.i18n();

  i18n.locale = idioma_app;

  cambioIdioma();

  if (seccion_app && seccion_app == 'fin') {
    window.location.href = 'orden-cepillado';
  } else {
    if (!seccion_app || seccion_app == 'idioma') {
      $('.Seccion-idioma').addClass('Seccion-activa');
    } else {
      if (seccion_app != 'idioma') {
        $('.Seccion-activa').removeClass('Seccion-activa');
        $('.Seccion-' + seccion_app).addClass('Seccion-activa');
        $('.Seccion-anterior').css('display', 'block');

        if (seccion_app == 'foto') {
          $('.Seccion-siguiente').addClass('Seccion-ultimo');
        }
      }
    }

    $('.Seccion-acciones').show();
  }

  if (!avatar_tipo) {
    avatar_tipo = 'chico';
    localStorage.setItem("avatar-app", avatar_tipo);
  }
  if (!avatar_id) {
    avatar_id = '1';
    localStorage.setItem("avatarid-app", avatar_id);
  }

  $('.Btn-color').on('click', function () {
    guardarcolorboton = $(this).prop('id').split('-');
    colorapp = guardarcolorboton[1];

    localStorage.setItem("color-app", colorapp);

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $(this).parent().addClass('Colorc-borde-' + colorapp);

    CambiardeFondo(colorapp);
  });

  $('.Select-idioma').on('click', function () {
    $('.Select-idioma').parent().removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $(this).parent().addClass('Colorc-borde-' + colorapp);

    idioma_app = $(this).attr('data-idioma');
    localStorage.setItem("idioma-app", idioma_app);
    i18n.locale = $(this).attr('data-idioma');
    cambioIdioma();
  });

  $('.Seccion-siguiente').on('click', function () {
    if ($(this).hasClass('Seccion-ultimo')) {
      $('[type=submit]', '#Perfil').click();
    } else {
      $('.Seccion-activa').removeClass('Seccion-activa').next('section').addClass('Seccion-activa');
      localStorage.setItem("seccion-app", $('.Seccion-activa').attr('data-seccion'));

      if (!$('.Seccion-anterior').is(':visible')) {
        $('.Seccion-anterior').css('display', 'block');
      }

      if ($('.Seccion-activa').attr('data-seccion') == 'foto') {
        $(this).addClass('Seccion-ultimo');
      }
    }
  });

  $('.Seccion-anterior').on('click', function () {
    $('.Seccion-activa').removeClass('Seccion-activa').prev('section').addClass('Seccion-activa');
    localStorage.setItem("seccion-app", $('.Seccion-activa').attr('data-seccion'));

    if ($('.Seccion-activa').attr('data-seccion') == 'idioma') {
      $(this).hide();
    }

    $('.Seccion-siguiente').removeClass('Seccion-ultimo');
  });

  $('.Avatar-seleccionar').on('click', function () {
    $('.Avatar-seleccionar').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $(this).addClass('Colorc-borde-' + colorapp);

    avatar_content = $(this);
    avatar_tipo = $(this).attr('data-avatartipo');

    if (avatar_tipo == 'chico') {
      MicroModal.show('modal-1');
    } else {
      MicroModal.show('modal-2');
    }
  });

  $('.Avatar-img').on('click', function () {
    $('.Avatar-img').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    }).removeClass('Avatar-activo');

    $(this).addClass('Colorc-borde-' + colorapp);
    $(this).addClass('Avatar-activo');
  });

  $('.Seleccionar-avatar').on('click', function () {
    avatar_id = $('.Avatar-activo').attr('data-avatar');
    $('img', avatar_content).attr('src', 'dist/assets/images/' + avatar_tipo + '-' + avatar_id + '.png');
    MicroModal.close('modal-1');
    MicroModal.close('modal-2');
  });

  $('.Tomar-foto').on('click', function () {
    $('.File-foto').click();
  });

  $('#Perfil').on('submit', function (e) {
    e.preventDefault();

    // if ($('.File-foto').val() != '') {
    //   inputFileImage = document.getElementById('file-imagen');
    //   file = inputFileImage.files[0];
    //   formData.append('foto', file);
    // } else {
      formData.append('registro[imagen]', avatar_tipo + '-' + avatar_id + '.png');
    // }

    data = $(this).serializeArray();
    $.each(data, function (key, input) {
      formData.append(input.name, input.value);
    });

    formData.append('registro[idioma]', idioma_app);
    formData.append('registro[color]', colorapp);
    formData.append('registro[opc]', 'registro');

    $.ajax('dist/libs/ac_registro', {
      method: "POST",
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      dataType: 'json',
      success: function (data) {
        if (data.status == true) {
          localStorage.setItem("usuariofoto-app", data.foto);
          localStorage.setItem("seccion-app", 'fin');
          if (data.foto == '') {
            localStorage.setItem("avatar-app", avatar_tipo);
            localStorage.setItem("avatarid-app", avatar_id);
          }

          localStorage.setItem("usuarioid-app", data.token);
          localStorage.setItem("celularid-app", data.celular);

          localStorage.setItem("usuario-app", $('.Nombre-perfil').val());
          window.location.href = 'orden-cepillado';
        } else {
          notification('error', data.msg);
        }
      }
    });

  });

  function CambiardeFondo(nuevocolor) {
    $.each($(".Colors"), function () {

      if ($(this).is("[class*='Colorc-bag-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-bag-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-text-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-text-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-text-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-borde-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-borde-' + nuevocolor);
      }

    })

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $('#C-' + nuevocolor).parent().addClass('Colorc-borde-' + nuevocolor);

    if (nuevocolor == 'gris' || nuevocolor == 'negro') {
      $('.Conten-avatar-int img').css('filter', 'grayscale(1)');
    } else {
      $('.Conten-avatar-int img').css('filter', '');
    }
  }

  function cambioIdioma() {
    i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
      $('*').i18n();
      $('.Nombre-perfil').attr('placeholder', $.i18n('disabrush-placeholder-nombre-perfil'));
    });
  }

  function notification(t, m) {
    var n = new Noty({
      type: t,
      text: m,
      layout: 'center',
      theme: 'relax',
      modal: true,
      closeWith: ['click'],
      progressBar: false,
      timeout: 1500,
    });
    n.show();
  }

});
