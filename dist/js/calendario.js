$(document).ready(function () {
  var colorapp, idioma_app, usuarioid_app, celularid_app;

  colorapp = localStorage.getItem('color-app');
  idioma_app = localStorage.getItem('idioma-app');
  usuarioid_app = localStorage.getItem('usuarioid-app');
  celularid_app = localStorage.getItem('celularid-app');

  if (colorapp) {
    CambiardeFondo(colorapp);
  } else {
    colorapp = 'azul';
    localStorage.setItem("color-app", colorapp);
  }

  if (!idioma_app) {
    idioma_app = 'es';
    localStorage.setItem("idioma-app", idioma_app);
  }

  $('.Time-pickr').flatpickr({
    enableTime: true,
    noCalendar: true,
  });

  $('.Date-pickr').flatpickr({
    locale: idioma_app,
    disableMobile: true
  });


  var i18n = $.i18n();

  i18n.locale = idioma_app;

  i18n.load('dist/i18n/' + i18n.locale + '.json', i18n.locale).done(function () {
    $('*').i18n();
    $('.Date-pickr').attr('placeholder', $.i18n('disabrush-placeholder-fecha'));
    $('[type=submit]').val($.i18n('disabrush-btn-guardar'));

    $('body').show();
  });

  $.post('dist/libs/ac_calendario', {
    'calendario[celularid]': celularid_app,
    'calendario[usuarioid]': usuarioid_app,
    'calendario[opc]': 'Recordatorios-cargar'
  }, function (data) {
    if (data.diario != '') {
      $('.Hora-1').val(data.diario[0].manana_rd);
      $('.Hora-2').val(data.diario[0].tarde_rd);
      $('.Hora-3').val(data.diario[0].noche_rd);
    }

    if (data.cambio != '') {
      $('.Fecha-cambio').val(data.cambio[0].fecha_rc);
    }

    if (data.cita != '') {
      $('.Fecha-cita').val(data.cita[0].fecha_rc);
    }
  }, 'json');

  $('.Seccion-padre').on('click', function () {
    $('.Seccion-hijo').addClass('Conten-oculto');
    $(this).next('.Seccion-hijo').removeClass('Conten-oculto');
  });

  $('#Diario').on('submit', function (e) {
    e.preventDefault();
    data = $(this).serializeArray();
    data.push({
      name: 'calendario[idioma]',
      value: idioma_app
    }, {
      name: 'calendario[celularid]',
      value: celularid_app
    }, {
      name: 'calendario[usuarioid]',
      value: usuarioid_app
    }, {
      name: 'calendario[opc]',
      value: 'Recordatorio-diario'
    });

    $.post('dist/libs/ac_calendario', data, function (data) {
      if (data.status == true) {
        notification('success', data.msg);
      } else {
        notification('error', data.msg);
      }
    }, 'json');
  });

  $('#Cambio').on('submit', function (e) {
    e.preventDefault();
    data = $(this).serializeArray();
    data.push({
      name: 'calendario[idioma]',
      value: idioma_app
    }, {
      name: 'calendario[celularid]',
      value: celularid_app
    }, {
      name: 'calendario[usuarioid]',
      value: usuarioid_app
    }, {
      name: 'calendario[opc]',
      value: 'Recordatorio-cambio'
    });

    $.post('dist/libs/ac_calendario', data, function (data) {
      if (data.status == true) {
        notification('success', data.msg);
      } else {
        notification('error', data.msg);
      }
    }, 'json');
  });

  $('#Cita').on('submit', function (e) {
    e.preventDefault();
    data = $(this).serializeArray();
    data.push({
      name: 'calendario[idioma]',
      value: idioma_app
    }, {
      name: 'calendario[celularid]',
      value: celularid_app
    }, {
      name: 'calendario[usuarioid]',
      value: usuarioid_app
    }, {
      name: 'calendario[opc]',
      value: 'Recordatorio-cita'
    });

    $.post('dist/libs/ac_calendario', data, function (data) {
      if (data.status == true) {
        notification('success', data.msg);
      } else {
        notification('error', data.msg);
      }
    }, 'json');
  });

  function CambiardeFondo(nuevocolor) {
    $.each($(".Colors"), function () {

      if ($(this).is("[class*='Colorc-bag-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-bag-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-bag-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-text-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-text-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-text-' + nuevocolor);
      }

      if ($(this).is("[class*='Colorc-borde-']")) {
        $(this).removeClass(function (index, css) {
          return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
        });

        $(this).addClass('Colorc-borde-' + nuevocolor);
      }

    })

    $('.Conten-colores-bloque').removeClass(function (index, css) {
      return (css.match(/\Colorc-borde-\S+/g) || []).join(' ');
    });

    $('#C-' + nuevocolor).parent().addClass('Colorc-borde-' + nuevocolor);
  }

  $('.Conten-bloque-menuabajo-sec a').on('click', function () {
    $('body').append('<div class="Fondo-cargando"><div class="Cargando-spin"><div class="lds-dual-ring"></div></div></div>');
  });

  function notification(t, m) {
    var n = new Noty({
      type: t,
      text: m,
      layout: 'center',
      theme: 'relax',
      modal: true,
      closeWith: ['click'],
      progressBar: false,
      timeout: 1500,
    });
    n.show();
  }

});
