<?php
require "conexion.php";
$data = $_REQUEST['contacto'];
$msg = array();

switch ($data['opc']) {
    case 'Envio-correo':
        $cabeceras = "From: noreply@disabrush.com";
        $asunto = "Mensaje contactanos";
        $email_to = "miguelucho2m4@gmail.com ";
        $contenido = "Nuevo mensaje \n"
            . "\n"
            . "Datos contacto \n\n"
            . "Nombre: $data[nombre] \n"
            . "Correo electrónico:  $data[email] \n"
            . "Mensaje:  $data[mensaje] \n"
            . "\n";
        if (@mail($email_to, $asunto, $contenido, $cabeceras)) {
            $msg['status'] = true;

            if ($data['idioma'] == 'en') {
                $msg['msg'] = 'Message send';
            } else {
                $msg['msg'] = 'Mensaje enviado';
            }
        } else {
            $msg['status'] = false;

            if ($data['idioma'] == 'en') {
                $msg['msg'] = 'The message could not be sent';
            } else {
                $msg['msg'] = 'No se pudo enviar el mensaje';
            }
        }

        echo json_encode($msg);

        break;

    case 'Envio-calificacion':
        $registros = $db
            ->where('token_navegador_rg', $data['token'])
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $nombre = $registros[0]->nombre_rg;
            $calificacion = $data['calificacion'];

            $datos = [
                'Id_rg' => $registros[0]->Id_rg,
                'calificacion_ca' =>  $calificacion
            ];

            $db
                ->insert('calificaciones', $datos);

            $cabeceras = "From: noreply@disabrush.com";
            $asunto = "Mensaje calificación app";
            $email_to = "miguelucho2m4@gmail.com ";
            $contenido = "Nuevo mensaje \n"
                . "\n"
                . "Datos Calificación \n\n"
                . "Nombre: $nombre \n"
                . "Calificación:  $calificacion \n"
                . "\n";

            if (mail($email_to, $asunto, $contenido, $cabeceras)) {
                $msg['status'] = true;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'Thank you, your rating has been sent.';
                } else {
                    $msg['msg'] = 'Gracias, tu calificación ha sido enviada.';
                }
            } else {
                $msg['status'] = false;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'Error';
                } else {
                    $msg['msg'] = 'Error';
                }
            }
        }

        echo json_encode($msg);
        break;
}
