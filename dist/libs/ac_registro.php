<?php
require "conexion.php";

$msg = array();
$data = $_REQUEST['registro'];

date_default_timezone_set("America/Bogota");
$folder_imagenes = '../assets/images';


switch ($data['opc']) {
    case 'registro':
        $msg['foto'] = '';
        $foto = 0;

        $token = hash('sha256', date('mdYhis') . $data['nombre'] . md5(uniqid(rand(), true)));

        $datos = [
            'nombre_rg' => $data['nombre'],
            'idioma_rg' => $data['idioma'],
            'color_rg' => $data['color'],
            'token_navegador_rg' => $token,
            'token_celular_rg' => $data['token_celular']
        ];

        if (isset($_FILES['foto'])) {
            $nombre_imagen = $_FILES['foto']['name'];
            $tmp_imagen    = $_FILES['foto']['tmp_name'];
            $ext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
            $nombre_imagen = limpiar(basename($_FILES['foto']['name'], "." . $ext)) . '.' . $ext;

            $nombredoc  = 'foto-' . date('mdYhis') . '-' . $nombre_imagen;
            $archivador = $folder_imagenes . '/' . $nombredoc;

            if (move_uploaded_file($tmp_imagen, $archivador)) {
                $archivador = substr($folder_imagenes, 3) . '/' . $nombredoc;
                $foto = 1;
                $datos['imagen_rg'] = $archivador;
            } else {
                $msg['status'] = false;
                $msg['msg'] = 'No se pudo realizar el registro';

                echo json_encode($msg);
                exit;
            }
        } else {
            $datos['imagen_rg'] = $data['imagen'];
        }

        $registro = $db
            ->insert('registros', $datos);

        if ($registro) {
            if ($foto == 1) {
                $msg['foto'] = $archivador;
            }

            $msg['status'] = true;
            $msg['token'] = $token;
            $msg['celular'] = $data['token_celular'];
        } else {
            $msg['status'] = false;
            $msg['msg'] = 'No se pudo realizar el registro';
        }

        echo json_encode($msg);
        break;
    case 'perfil':
        $msg['foto'] = '';
        $foto = 0;

        $registros = $db
            ->where('token_navegador_rg', $data['token'])
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $datos = [
                'nombre_rg' => $data['nombre'],
                'color_rg' => $data['color'],
            ];

            if (isset($_FILES['foto'])) {
                $nombre_imagen = $_FILES['foto']['name'];
                $tmp_imagen    = $_FILES['foto']['tmp_name'];
                $ext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
                $nombre_imagen = limpiar(basename($_FILES['foto']['name'], "." . $ext)) . '.' . $ext;

                $nombredoc        = 'foto-' . date('mdYhis') . '-' . $nombre_imagen;
                $archivador       = $folder_imagenes . '/' . $nombredoc;

                if (move_uploaded_file($tmp_imagen, $archivador)) {
                    $archivador = substr($folder_imagenes, 3) . '/' . $nombredoc;
                    $foto = 1;
                    $datos['imagen_rg'] = $archivador;

                    if ($registros[0]->imagen_rg != '') {
                        if (file_exists('../' . $registros[0]->imagen_rg)) {
                            unlink('../' . $registros[0]->imagen_rg);
                        }
                    }
                } else {
                    $msg['status'] = false;

                    if ($data['idioma'] == 'en') {
                        $msg['msg'] = 'The profile can\'t be updated';
                    } else {
                        $msg['msg'] = 'No se pudo editar el perfil';
                    }

                    echo json_encode($msg);
                    exit;
                }
            } else {
                $datos['imagen_rg'] = $data['imagen'];
            }

            $registro = $db
                ->where('token_navegador_rg', $data['token'])
                ->update('registros', $datos);

            if ($registro) {
                if ($foto == 1) {
                    $msg['foto'] = $archivador;
                }

                $msg['status'] = true;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'The profile has been updated';
                } else {
                    $msg['msg'] = 'El perfil ha sido actualizado';
                }
            } else {
                $msg['status'] = false;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'The profile can\'t be updated';
                } else {
                    $msg['msg'] = 'No se pudo editar el perfil';
                }
            }
        } else {
            $msg['status'] = false;

            if ($data['idioma'] == 'en') {
                $msg['msg'] = 'The profile can\'t be updated';
            } else {
                $msg['msg'] = 'No se pudo editar el perfil';
            }
        }

        echo json_encode($msg);
        break;
}

function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
