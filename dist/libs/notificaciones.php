<?php
require "conexion.php";
date_default_timezone_set("America/Bogota");

$key = 'AAAAfk9E4vc:APA91bH5D1WgpryaC3hEfqRQVvVVpr7rE5_hsg62onijvLIBoww85xW1s0C9nAvGq2Uk7DFZMFtMC7_3b0gJ02oOG4jRmKVi0nO7ZUaXsApWc-_TeToUwlNxLS-UQEXGneMrEfYxNMT_';

$fecha = date('Y-m-d');

//! Cambio cepillo

$recordatorios = $db
    ->where('fecha_rc', $fecha)
    ->objectBuilder()->get('recordatorios_cepillo');

if ($db->count > 0) {
    foreach ($recordatorios as $recordatorio) {
        $registros = $db
            ->where('Id_rg', $recordatorio->Id_rg)
            ->where('token_celular_rg', '', '!=')
            ->objectBuilder()->get('registros');

        foreach ($registros as $registro) {
            $token = array($registro->token_celular_rg);

            $fields = array(
                'registration_ids' => $token,
                'priority'         => 10,
                'notification'     => array('title' => 'Disabrush', 'body' => 'Recordatorio - hoy tienes que cambiar tu cepillo', 'sound' => 'Default', 'image' => 'Notification Image'),
            );

            $headers = array(
                'Authorization:key=' . $key,
                'Content-Type:application/json',
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);

            $respuesta = json_decode($result);
        }
    }
}


//! Cita

$recordatorios = $db
    ->where('fecha_rc', $fecha)
    ->objectBuilder()->get('recordatorios_cita');

if ($db->count > 0) {
    foreach ($recordatorios as $recordatorio) {
        $registros = $db
            ->where('Id_rg', $recordatorio->Id_rg)
            ->where('token_celular_rg', '', '!=')
            ->objectBuilder()->get('registros');

        foreach ($registros as $registro) {
            $token = array($registro->token_celular_rg);

            $fields = array(
                'registration_ids' => $token,
                'priority'         => 10,
                'notification'     => array('title' => 'Disabrush', 'body' => 'Recordatorio - Hoy tienes cita odontologica', 'sound' => 'Default'),
            );

            $headers = array(
                'Authorization:key=' . $key,
                'Content-Type:application/json',
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);

            $respuesta = json_decode($result);
        }
    }
}
