<?php
require "conexion.php";

$msg = array();
$data = $_REQUEST['calendario'];

date_default_timezone_set("America/Bogota");


switch ($data['opc']) {
    case 'Recordatorio-diario':
        $registros = $db
            ->where('token_celular_rg', $data['celularid'])
            ->orWhere('token_navegador_rg', $data['usuarioid'])
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $datos = [
                'manana_rd' => $data['hora1'],
                'tarde_rd' => $data['hora2'],
                'noche_rd' => $data['hora3'],
            ];

            $recordatorios = $db
                ->where('Id_rg', $registros[0]->Id_rg)
                ->objectBuilder()->get('recordatorios_diarios');

            if ($db->count > 0) {
                $recordatorio = $db
                    ->where('Id_rg', $registros[0]->Id_rg)
                    ->update('recordatorios_diarios', $datos);
            } else {
                $datos['Id_rg'] = $registros[0]->Id_rg;

                $recordatorio = $db
                    ->insert('recordatorios_diarios', $datos);
            }

            if ($recordatorio) {
                $msg['status'] = true;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'The reminder has been saved';
                } else {
                    $msg['msg'] = 'El recordatorio se ha guardado';
                }
            } else {
                $msg['status'] = false;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'Failed to register';
                } else {
                    $msg['msg'] = 'No se pudo realizar el registro';
                }
            }
        } else {
            $msg['status'] = false;

            if ($data['idioma'] == 'en') {
                $msg['msg'] = 'Failed to register';
            } else {
                $msg['msg'] = 'No se pudo realizar el registro';
            }
        }

        echo json_encode($msg);
        break;
    case 'Recordatorio-cambio':
        $registros = $db
            ->where('token_celular_rg', $data['celularid'])
            ->orWhere('token_navegador_rg', $data['usuarioid'])
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $datos = [
                'fecha_rc' => $data['fecha'],
            ];

            $recordatorios = $db
                ->where('Id_rg', $registros[0]->Id_rg)
                ->objectBuilder()->get('recordatorios_cepillo');

            if ($db->count > 0) {
                $recordatorio = $db
                    ->where('Id_rg', $registros[0]->Id_rg)
                    ->update('recordatorios_cepillo', $datos);
            } else {
                $datos['Id_rg'] = $registros[0]->Id_rg;

                $recordatorio = $db
                    ->insert('recordatorios_cepillo', $datos);
            }

            if ($recordatorio) {
                $msg['status'] = true;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'The reminder has been saved';
                } else {
                    $msg['msg'] = 'El recordatorio se ha guardado';
                }
            } else {
                $msg['status'] = false;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'Failed to register';
                } else {
                    $msg['msg'] = 'No se pudo realizar el registro';
                }
            }
        } else {
            $msg['status'] = false;

            if ($data['idioma'] == 'en') {
                $msg['msg'] = 'Failed to register';
            } else {
                $msg['msg'] = 'No se pudo realizar el registro';
            }
        }

        echo json_encode($msg);
        break;
    case 'Recordatorio-cita':
        $registros = $db
            ->where('token_celular_rg', $data['celularid'])
            ->orWhere('token_navegador_rg', $data['usuarioid'])
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $datos = [
                'fecha_rc' => $data['fecha'],
            ];

            $recordatorios = $db
                ->where('Id_rg', $registros[0]->Id_rg)
                ->objectBuilder()->get('recordatorios_cita');

            if ($db->count > 0) {
                $recordatorio = $db
                    ->where('Id_rg', $registros[0]->Id_rg)
                    ->update('recordatorios_cita', $datos);
            } else {
                $datos['Id_rg'] = $registros[0]->Id_rg;

                $recordatorio = $db
                    ->insert('recordatorios_cita', $datos);
            }

            if ($recordatorio) {
                $msg['status'] = true;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'The reminder has been saved';
                } else {
                    $msg['msg'] = 'El recordatorio se ha guardado';
                }
            } else {
                $msg['status'] = false;

                if ($data['idioma'] == 'en') {
                    $msg['msg'] = 'Failed to register';
                } else {
                    $msg['msg'] = 'No se pudo realizar el registro';
                }
            }
        } else {
            $msg['status'] = false;

            if ($data['idioma'] == 'en') {
                $msg['msg'] = 'Failed to register';
            } else {
                $msg['msg'] = 'No se pudo realizar el registro';
            }
        }

        echo json_encode($msg);
        break;
    case 'Recordatorios-cargar':
        $msg['diario'] = [];
        $msg['cambio'] = [];
        $msg['cita'] = [];

        $registros = $db
            ->where('token_celular_rg', $data['celularid'])
            ->orWhere('token_navegador_rg', $data['usuarioid'])
            ->objectBuilder()->get('registros');

        if ($db->count > 0) {
            $recordatorios = $db
                ->where('Id_rg', $registros[0]->Id_rg)
                ->objectBuilder()->get('recordatorios_diarios');

            if ($db->count > 0) {
                $msg['diario'][] = $recordatorios[0];
            }

            $recordatorios = $db
                ->where('Id_rg', $registros[0]->Id_rg)
                ->objectBuilder()->get('recordatorios_cepillo');

            if ($db->count > 0) {
                $msg['cambio'][] = $recordatorios[0];
            }

            $recordatorios = $db
                ->where('Id_rg', $registros[0]->Id_rg)
                ->objectBuilder()->get('recordatorios_cita');

            if ($db->count > 0) {
                $msg['cita'][] = $recordatorios[0];
            }
        }

        echo json_encode($msg);
        break;
}
