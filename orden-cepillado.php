<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Disabrush</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <style>
    body {
      display: none;
    }

    .Seccion-inicio,
    .Seccion-cancion,
    .Seccion-paso1,
    .Seccion-paso2,
    .Seccion-paso3,
    .Seccion-paso4,
    .Seccion-paso5,
    .Seccion-paso6,
    .Seccion-paso7,
    .Seccion-paso8,
    .Seccion-paso9 {
      display: none;
    }

    .Seccion-activa {
      display: block;
    }

    .Seccion-paso2 .Conten-orden-superior-int,
    .Seccion-paso4 .Conten-orden-superior-int,
    .Seccion-paso6 .Conten-orden-superior-int {
      width: 240px;
    }

    .Seccion-paso7 .Conten-orden-superior-int {
      width: 200px;
    }
  </style>
</head>

<body>
  <section class="Seccion-inicio Seccion-activa">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-cepillado-inicio"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/diente.svg" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-cepillado-inicio"></a>
            </div>
            <div class="Conten-orden-inferior"></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Selección de la musica -->

  <section class="Seccion-cancion">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-cancion"></h2>
          <div class="Conten-selector-canciones">
            <div class="Conten-selector-canciones-bloque Colors Seleccion-cancion" data-src="cancion-1">
              <div class="Conten-selector-canciones-bloque-sec1">
                <i class="icon-play2"></i>
              </div>
              <div class="Conten-selector-canciones-bloque-sec2">
                <span class="letra-cancion">Tattoo Remix - Rauw Alejandro Ft Camilo</span>
              </div>
            </div>
            <div class="Conten-selector-canciones-bloque Colors Seleccion-cancion" data-src="cancion-2">
              <div class="Conten-selector-canciones-bloque-sec1">
                <i class="icon-play2"></i>
              </div>
              <div class="Conten-selector-canciones-bloque-sec2">
                <span class="letra-cancion">Dakiti - Bad Bunny ft Jhay Cortez</span>
              </div>
            </div>
            <div class="Conten-selector-canciones-bloque Colors Seleccion-cancion" data-src="cancion-3">
              <div class="Conten-selector-canciones-bloque-sec1">
                <i class="icon-play2"></i>
              </div>
              <div class="Conten-selector-canciones-bloque-sec2">
                <span class="letra-cancion">Erick Esquivel - Las locuras mías/ Silvestre Dangond</span>
              </div>
            </div>
            <div class="Conten-selector-canciones-bloque Colors Seleccion-cancion" data-src="cancion-4">
              <div class="Conten-selector-canciones-bloque-sec1">
                <i class="icon-play2"></i>
              </div>
              <div class="Conten-selector-canciones-bloque-sec2">
                <span class="letra-cancion">Tu cumpleaños - Acoustic Cover</span>
              </div>
            </div>
            <div class="Conten-selector-canciones-bloque Colors Seleccion-cancion" data-src="cancion-5">
              <div class="Conten-selector-canciones-bloque-sec1">
                <i class="icon-play2"></i>
              </div>
              <div class="Conten-selector-canciones-bloque-sec2">
                <span class="letra-cancion">Tarde lo conocí - Patricia Teheran Cover</span>
              </div>
            </div>
            <div class="Conten-selector-canciones-bloque Colors Seleccion-cancion" data-src="cancion-6">
              <div class="Conten-selector-canciones-bloque-sec1">
                <i class="icon-play2"></i>
              </div>
              <div class="Conten-selector-canciones-bloque-sec2">
                <span class="letra-cancion">Bichota Cover - Karol G</span>
              </div>
            </div>
          </div>
          <div class="Conten-orden-medio Padd-top Text-center">
            <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso1" data-paso="1">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-1"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/preparar_1.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="10"><span>00: 10</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso2" data-paso="2">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-2"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <!-- <img src="dist/assets/images/diente.svg" alt=""> -->
                <img src="dist/assets/images/cepillado-11.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="35"><span>00: 35</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso3" data-paso="3">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-3"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/escupir2.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="5"><span>00: 05</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso4" data-paso="4">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-4"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <!-- <img src="dist/assets/images/diente.svg" alt=""> -->
                <img src="dist/assets/images/cepillado-22.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="35"><span>00: 35</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso5" data-paso="5">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-3"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/escupir2.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="5"><span>00: 05</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso6" data-paso="6">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-5"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <!-- <img src="dist/assets/images/diente.svg" alt=""> -->
                <img src="dist/assets/images/cepillado-33.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="20"><span>00: 20</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso7" data-paso="7">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-6"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/cepillado-44.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="10"><span>00: 10</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso8" data-paso="8">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-3"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/escupir2.gif" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2" data-tiempo="5"><span>00: 05</span></div>
                </div>
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top Text-center">
              <div><a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-siguiente" data-i18n="disabrush-btn-siguiente"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="Seccion-paso9" data-paso="9">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-orden">
          <h2 data-colores="Texto-color" class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-paso-7"></h2>
          <div class="Conten-orden-sec">
            <div class="Conten-orden-superior">
              <div class="Conten-orden-superior-int Padd-top">
                <img src="dist/assets/images/aplaudir.svg" alt="">
              </div>
            </div>
            <div class="Conten-orden-medio Padd-top-gr Text-center">
              <!-- <div data-colores="Borde" class="Crometro Colors Colorc-borde-azul">
                <div class="Crometro-int">
                  <div class="Crometro-sec1"><i class="icon-clock"></i></div>
                  <div class="Crometro-sec2"><span>00: 35</span></div>
                </div>
              </div> -->
            </div>
            <div class="Conten-orden-inferior Padd-top-gr Text-center">
              <a data-colores="Fondo" class="Btn Text-center Des-block Text-blanco Colors Colorc-bag-azul Seccion-terminar" data-i18n="disabrush-btn-cepillado-fin"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Menu inferior flotante -->
  <?php include("dist/libs/menu-inferior.php") ?>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.js?<?php echo time()  ?>"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.messagestore.js"></script>
  <script src="dist/js/orden-cepillado.js?<?php echo time()  ?>"></script>

</body>

</html>