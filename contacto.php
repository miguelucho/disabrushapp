<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Disabrush</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <style>
    body {
      display: none;
    }

    .Text-agradecimientos p {
      margin: 0;
    }

    .Conten-global,
    .Conten-global-int {
      height: 190vh;

    }
  </style>
</head>

<body>
  <section>
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-ingresar-perfil">
          <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-titulo-contacto"></h2>
          <div class="Conten-ingresar-perfil-int">
            <form id="Contacto" class="Forms">
              <label for="" data-i18n="disabrush-label-nombre-contacto"></label>
              <input type="text" class="Colors Colorc-borde-azul Nombre-contacto" placeholder="" data-i18n="disabrush-placeholder-nombre-contacto" name="contacto[nombre]" required>
              <label for="" data-i18n="disabrush-label-email-contacto"></label>
              <input type="email" class="Colors Colorc-borde-azul Email-contacto" placeholder="" data-i18n="disabrush-placeholder-email-contacto" name="contacto[email]" required>
              <label for="" data-i18n="disabrush-label-mensaje-contacto"></label>
              <textarea placeholder="" class="Colors Colorc-borde-azul Mensaje-contacto" id="" cols="30" rows="10" name="contacto[mensaje]" required></textarea>
              <input type="submit" class="Btn Colors Colorc-bag-azul Text-blanco" data-i18n="disabrush-btn-enviar" value="">
            </form>
          </div>
          <br>
          <hr class="Divi">
          <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-calificar"></h2>
          <div class="Conten-estrellas">
            <div class="Conten-estrellas-int">
              <span class="icon-star-empty"></span>
              <span class="icon-star-empty"></span>
              <span class="icon-star-empty"></span>
              <span class="icon-star-empty"></span>
              <span class="icon-star-empty"></span>
            </div>
          </div>
          <br>
          <hr class="Divi">
          <div class="Text-agradecimientos">
            <p class="Parrafo" style="text-align: justify;" data-i18n="disabrush-texto-agradecimiento"></p>
            <br>
            <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-asesor" style="font-size: 20px;"></h2>
            <p class="Parrafo Text-center">Johanna Carolina Arias Ramirez</p>
            <p class="Parrafo Text-center">Claudia Patricia Rodas Avellaneda</p>
            <p class="Parrafo Text-center">Saulo Andres Olarte Buritica</p>
            <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-label-estudiante" style="font-size: 20px;"></h2>
            <p class="Parrafo Text-center">Diana Yiseth Mendoza Ladino</p>
            <p class="Parrafo Text-center">Yeicy Norelly Daza Estrada</p>
            <p class="Parrafo Text-center">Yuli Paola Bohorquez Hurtado</p>
            <p class="Parrafo Text-center">Yurani Vanessa Silva Ocampo</p>
          </div>
          <div class="Conten-logo-uni">
            <img src="dist/assets/images/logoUCC.jpeg" alt="">
          </div>
        </div>

        <!-- Menu inferior flotante -->
        <?php include("dist/libs/menu-inferior.php") ?>

      </div>
  </section>
  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/noty.min.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.js?<?php echo time()  ?>"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.messagestore.js"></script>
  <script src="dist/js/contacto.js?<?php echo time()  ?>"></script>
</body>

</html>