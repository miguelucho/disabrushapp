<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Disabrush</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <style>
    body {
      display: none;
    }
  </style>
</head>

<body>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v11.0" nonce="LPJTYYcZ"></script>
  <section>
    <div class="Conten-global">
      <div class="Conten-global-int">
        <div class="Conten-ingresar-perfil">
          <h2 class="Titul-h2 Text-center Colors Colorc-text-azul" data-i18n="disabrush-titulo-compartir"></h2>
          <div class="Conten-compartir">
            <div class="Conten-compartir-int">
              <ul>
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.disabrush&amp;src=sdkpreparse"><i data-colores="Texto" class="icon-facebook Colors Colorc-text-azul"></i> Facebook</a></li>
                <li><a href="https://twitter.com/intent/tweet?text=Descarga%20Disabrush%20https://play.google.com/store/apps/details?id=com.disabrush"><i data-colores="Texto" class="icon-twitter Colors Colorc-text-azul"></i> Twitter</a></li>
                <li><a href="whatsapp://send?text=https://play.google.com/store/apps/details?id=com.disabrush"><i data-colores="Texto" class="icon-whatsapp Colors Colorc-text-azul"></i> WhatsApp</a></li>
              </ul>
            </div>
          </div>
        </div>

        <!-- Menu inferior flotante -->
        <?php include("dist/libs/menu-inferior.php") ?>

      </div>
  </section>
  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.js"></script>
  <script src="dist/js/jquery.i18n/jquery.i18n.messagestore.js"></script>
  <script src="dist/js/compartir.js"></script>
</body>

</html>